import mangopaysdk

from . import settings

mangopaysdk.client_id = settings.MANGOPAY_CLIENT_ID
mangopaysdk.apikey = settings.MANGOPAY_APIKEY

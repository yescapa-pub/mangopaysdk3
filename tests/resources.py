from . import settings

from mangopaysdk.api import APIRequest

handler = APIRequest(client_id=settings.MANGOPAY_CLIENT_ID,
                     apikey=settings.MANGOPAY_APIKEY,
                     sandbox=settings.MANGOPAY_USE_SANDBOX)

from mangopaysdk.resources import *  # noqa
